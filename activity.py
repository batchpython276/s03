class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program.")
    
    def info(self):
        print(f"I am {self.name} of batch {self.batch}.")

zuitt_camper = Camper("Nidz", "Batch 2023", "Intro to Python")

# print(zuitt_camper.name)
# print(zuitt_camper.batch)
# print(zuitt_camper.course_type)

# zuitt_camper.info()
# zuitt_camper.career_track()

print(f"I am {zuitt_camper.name} of batch {zuitt_camper.batch}. Currently enrolled in the {zuitt_camper.course_type} program.")

