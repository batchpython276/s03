# Python has several structures to store collections or multiple items in a single variable

# List[], dictionary, tuples(), set{}
# Tuples - ("MATH", "Science", "ENG")
# Set - {"MATH", "SCI", "ENG"}


# [Section] Lists
# Lists are similar to arrays of JS in a sense that they can contain a collection of data
# To create a list, the square bracket([]) is used.

names = ["John", "George", "Ringo"] #Stringlist

programs = ["developer career", "pi-shape", "short courses"] #string lists
durations = [260, 180, 20] #number list
truth_var = [True, False, True, True, False] #boolean list

# A list can contain elements with different data types:
sample_list = ["Apple", 3, False, "Potato", 4, False]

# Note: Problems may arise if you try to use list with multiple types for something that expects them to be the same type

# getting the list size
# the number of elements in a list can be counted using the len() method

print(len(programs))

var_len = len(sample_list)

print(var_len)

# Accessing values/elemets in a list
# list can be accessed by providing the index number of the element
# it can be accessed using the negative index

print(names[0])
print(names[-1])
print(programs[1])


print(programs)


# Access a range of values
print(programs[0:1])

# Updating lists
print(f"Current value: {programs[2]}")

programs[2] = 'ShortCourses'
print(f"New value: {programs[2]}")


# [Section] list manipulation
# List has methods that can be used to manipulate the elements within
# Adding list items - the append() method allows to insert items to a list

programs.append('global')
print(programs)

# Deleting Items - the "del" keyword can be used to delete elements in the list

durations.append(360)
print(durations)

del durations[-1]
print(durations)

# insert method
durations.insert(0, 100)
print(durations)


# Membership checking - the "in" keyword checks if the elements is in the list
# returns True or False value

print(20 in durations)
print(120 in durations)


# Sort list = the sort() method sorts the list alphanumerically, ascending, by default.

durations.sort()
print(durations)


# emptying the list - the clear() method is used to empty the contents of the list
test_list = [1,3,5,7,9]

print(test_list)
test_list.clear()
print(test_list)


# [Section] Dictionaries are used to store data values in key:value pairs. this is similar to objects in JS
# A dictionary is a collection which is ordered, changeable and does not allow duplicates
# To create a dictionary, the curly braces ({}) is used and the key:value pairs are denoted with (key: value)


person1 = {
	"name" : "Brandon",
	"age" : 28,
	"Occupation" : "Student",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "DJANGO"]
}


# To get the number of key-value pairs on the dictionary, the len() method
print(len(person1))


# Accessing values in dictionary
# To get item in the dictionary, the key name can be referred using square bracket([])

print(person1["name"])


print(person1.values())



print(person1)
print(person1.items())


# Adding key-value pairs can be done with either a new index key and assigning a value or the update method 
# index key
person1["nationality"] = "Filipino"
print(person1)


# update method
person1.update({"fav_food" : "Sinigang"})
print(person1)

# Deleting entries - it can be done using the pop() method and the del keyword

person1.pop("fav_food")
print(person1)

# using the del keyword
del person1["nationality"]
print(person1)

# The clear() method empties the dictionary
person2 = {
	"name" : "john",
	"age" : 18
}

person2.clear()
print(person2)


# Looping through dictionary

for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested dictionaries - dictionaries can be nested inside each other

person3 = {
	"name" : "Monika",
	"age" : 20,
	"Occupation" : "poet",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "DJANGO"]
}


print(person3["subjects"][0])
print(person3["subjects"][-1])

class_room = {
	"student1" : person1,
	"student2" : person3
}

print(class_room)
print(class_room.items())
print(class_room["student2"]["subjects"][1])


# [Section] Functions
# functions are block of code that runs when called.
# A function can be used to get inputs, process inputs and the "def" keyword is used to create a function.
# def <function_name>()


# defines a function called my_greeting
def my_greeting():
	print("hello user!")

my_greeting()


# Parameters can be added to functions to have more control to what the inputs for the function will be.

def greet_user(username):
	print(f'Hello, {username}')



# Arguments are the values that are submitted to the parameters
greet_user("Nidz")

# Return statements - the "return" keyword allow functions to return values

def addition(num1, num2):
	return num1 + num2

sum = addition(5, 9)
print(f"The sum of num1 and num2 is {sum}")
print(addition(10,20))

# [Section] Lambda Function
# A lambda function is a small anonymous function that can be used for callbacks
# It is just like any normalpython function, except that it has no name when defining it, and it is contained in one of code


# A lambda function can take any number of arguments but can only have one expression


greeting = lambda personahe: print(f'hello {personahe}')

greeting("JUNIIOR")


mult = lambda a, b: a*b

print(mult(10,6))


# [Section] Classes
# Classes would serve as blueprints to describe the concept to objects
# Each Object has the characteristics (properties) and behaviors (method)


# To create a Class, the "class" keyword is used along with the class name that starts with an uppercase character

class Car():
	# properties that all car Car objects must have are defines in a method called init

	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# properties HARD CODED
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# method
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print('filling pu the fuel tank. . . ')
		self.fuel_level = 100
		print(f'NEw fuel level: {self.fuel_level}')

	def drive(self, distance):
		print(f"The car has driven {distance} kms1")

		self.fuel_level = self.fuel_level - distance
		print(f"The fuel left: {self.fuel_level}")


# instantiate a class
new_car = Car("Nissan", "GT-R", 2019)


print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

new_car.fill_fuel()

new_car.drive(50)











